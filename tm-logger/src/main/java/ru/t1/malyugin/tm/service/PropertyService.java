package ru.t1.malyugin.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.IPropertyService;

@Getter
@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    @Value("${QUEUE_NAME:TM_LOGGER}")
    private String queueName;

    @NotNull
    @Value("${QUEUE_URL}")
    private String queueUrl;

    @NotNull
    @Value("${MONGO_HOST:localhost}")
    private String mongoHost;

    @NotNull
    @Value("${MONGO_PORT:27017}")
    private String mongoPort;

    @NotNull
    @Value("${MONGO_NAME:TM_LOG_DEFAULT}")
    private String mongoDBName;

}