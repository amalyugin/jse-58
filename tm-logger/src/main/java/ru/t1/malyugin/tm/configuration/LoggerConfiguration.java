package ru.t1.malyugin.tm.configuration;

import com.mongodb.MongoClient;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.malyugin.tm.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.malyugin.tm")
public class LoggerConfiguration {

    @NotNull
    private final IPropertyService propertyService;

    @Autowired
    public LoggerConfiguration(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Bean
    public ConnectionFactory factory() {
        @NotNull final String queueUrl = propertyService.getQueueUrl();
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(queueUrl);
        return connectionFactory;
    }

    @Bean
    @Scope("prototype")
    public MongoClient mongoClient() {
        @NotNull String host = propertyService.getMongoHost();
        @NotNull String port = propertyService.getMongoPort();
        return new MongoClient(host, Integer.parseInt(port));
    }

}