package ru.t1.malyugin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.event.ConsoleEvent;
import ru.t1.malyugin.tm.listener.AbstractListener;

import java.util.Arrays;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String DESCRIPTION = "Show command list";

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        Arrays.stream(abstractListeners).map(l -> l.getName() + ": " + l.getDescription()).forEach(System.out::println);
    }

}