package ru.t1.malyugin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.dto.request.system.ServerAboutRequest;
import ru.t1.malyugin.tm.dto.response.system.ServerAboutResponse;
import ru.t1.malyugin.tm.event.ConsoleEvent;

@Component
public final class ServerAboutListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "about-server";

    @NotNull
    private static final String DESCRIPTION = "Server about";

    @NotNull
    private static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@serverAboutListener.getName() == #event.message")
    public void handleConsoleEvent(@NotNull final ConsoleEvent event) {
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        @Nullable final ServerAboutResponse response = systemEndpoint.getAbout(request);

        @Nullable final String serverName = response.getServerName();
        @Nullable final String authorName = response.getAuthorName();
        @Nullable final String authorEmail = response.getAuthorEmail();
        @Nullable final String gitBranch = response.getGitBranch();
        @Nullable final String gitCommitId = response.getGitCommitId();
        @Nullable final String gitCommitterName = response.getGitCommitterName();
        @Nullable final String gitCommitterEmail = response.getGitCommitterEmail();
        @Nullable final String gitMessage = response.getGitMessage();
        @Nullable final String gitTime = response.getGitTime();

        System.out.println("[SERVER INFORMATION]");
        System.out.println("SERVER NAME: " + serverName);
        System.out.println("AUTHOR NAME: " + authorName);
        System.out.println("AUTHOR E-MAIL: " + authorEmail);

        System.out.println("\n[GIT INFORMATION]");
        System.out.println("BRANCH: " + gitBranch);
        System.out.println("COMMIT ID: " + gitCommitId);
        System.out.println("COMMITTER NAME: " + gitCommitterName);
        System.out.println("COMMITTER EMAIL: " + gitCommitterEmail);
        System.out.println("MESSAGE: " + gitMessage);
        System.out.println("TIME: " + gitTime);
    }

}