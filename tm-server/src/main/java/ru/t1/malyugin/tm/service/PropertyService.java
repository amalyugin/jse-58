package ru.t1.malyugin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['application.name']}")
    private String applicationName;

    @Value("#{environment['application.log']}")
    private String applicationLogDir;

    @Value("#{environment['application.dump']}")
    private String applicationDumpDir;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['database.username']}")
    private String databaseUsername;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['database.liquibase_config']}")
    private String liquibaseConfigPath;

    @Value("#{environment['database.init_token']}")
    private String liquibaseInitToken;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String cacheUseSecondLevel;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String cacheUseQuery;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String cacheUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String cacheRegionPrefix;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String cacheConfigFile;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String cacheRegionFactoryClass;

    @Value("#{environment['jms.logger_url']}")
    private String jmsQueueUrl;

    @Value("#{environment['jms.logger_queue']}")
    private String jmsQueueName;

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String GIT_BRANCH = "gitBranch";

    @NotNull
    private static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    private static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    private static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @NotNull
    private static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    private static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    private static final String EMPTY_VALUE = "---";


    @NotNull
    @Override
    public String getApplicationVersion() {
        return readManifest(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return readManifest(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return readManifest(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return readManifest(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return readManifest(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return readManifest(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return readManifest(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return readManifest(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return readManifest(GIT_COMMIT_TIME);
    }

    @NotNull
    private String readManifest(@Nullable final String key) {
        if (StringUtils.isBlank(key)) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}