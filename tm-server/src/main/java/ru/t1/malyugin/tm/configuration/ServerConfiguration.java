package ru.t1.malyugin.tm.configuration;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.model.TaskDTO;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.listener.EntityListener;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ComponentScan("ru.t1.malyugin.tm")
public class ServerConfiguration {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    @Bean
    @SneakyThrows
    public EntityManagerFactory entityManagerFactory(
            @NotNull final IPropertyService propertyService,
            @NotNull final EntityListener entityListener
    ) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(Environment.URL, propertyService.getDatabaseUrl());
        settings.put(Environment.USER, propertyService.getDatabaseUsername());
        settings.put(Environment.PASS, propertyService.getDatabasePassword());
        settings.put(Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());

        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getCacheUseSecondLevel());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getCacheUseQuery());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getCacheUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheConfigFile());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactoryClass());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);

        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        @NotNull final EntityManagerFactory factory = metadata.getSessionFactoryBuilder().build();

        initListeners(factory, entityListener);
        return factory;
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @Bean
    @NotNull
    public Liquibase liquibase(@NotNull final IPropertyService propertyService) throws IOException, SQLException, DatabaseException {
        @NotNull final Properties properties = new Properties();
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password"));
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @Nullable Database DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);

        @NotNull final String filename = propertyService.getLiquibaseConfigPath();
        return new Liquibase(filename, ACCESSOR, DATABASE);
    }

    private void initListeners(@NotNull final EntityManagerFactory factory, @NotNull final EntityListener entityListener) {
        @NotNull final SessionFactoryImpl sessionFactory = factory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        registry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

    @Bean
    @NotNull
    public ConnectionFactory jmsConnectionFactory(@NotNull final IPropertyService propertyService) {
        @NotNull final String url = propertyService.getJmsQueueUrl();
        @Nullable ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        return connectionFactory;
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public javax.jms.Connection jmsConnection(@NotNull final ConnectionFactory jmsConnectionFactory) throws JMSException {
        @NotNull final javax.jms.Connection connection = jmsConnectionFactory.createConnection();
        connection.start();
        return connection;
    }

}