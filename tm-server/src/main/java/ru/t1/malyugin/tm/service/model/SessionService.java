package ru.t1.malyugin.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.repository.model.ISessionRepository;
import ru.t1.malyugin.tm.api.service.model.ISessionService;
import ru.t1.malyugin.tm.model.Session;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session> implements ISessionService {

    @NotNull
    @Override
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

}