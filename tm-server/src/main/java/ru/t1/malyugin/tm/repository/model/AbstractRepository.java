package ru.t1.malyugin.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.repository.model.IRepository;
import ru.t1.malyugin.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<M> getClazz();

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return;
        remove(model);
    }

    @Override
    public void clear() {
        @NotNull final List<M> modelList = findAll();
        modelList.forEach(this::remove);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}