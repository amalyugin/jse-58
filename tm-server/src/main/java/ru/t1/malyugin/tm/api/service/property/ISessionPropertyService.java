package ru.t1.malyugin.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface ISessionPropertyService {

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}