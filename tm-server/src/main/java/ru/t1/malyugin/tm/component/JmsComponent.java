package ru.t1.malyugin.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;

import javax.annotation.PostConstruct;
import javax.jms.*;

@Component
public class JmsComponent {

    @NotNull
    private final IPropertyService propertyService;
    @NotNull
    private final Connection jmsConnection;
    @Getter
    private Session session;
    @Getter
    private MessageProducer messageProducer;

    @Autowired
    public JmsComponent(
            @NotNull final IPropertyService propertyService,
            @NotNull final Connection jmsConnection
    ) {
        this.propertyService = propertyService;
        this.jmsConnection = jmsConnection;
    }

    @PostConstruct
    private void initJmsComponent() throws JMSException {
        @NotNull final String queue = propertyService.getJmsQueueName();
        session = jmsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(queue);
        messageProducer = session.createProducer(destination);
    }

}