package ru.t1.malyugin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.malyugin.tm.api.service.dto.ISessionDTOService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO> implements ISessionDTOService {

    @NotNull
    @Override
    protected ISessionDTORepository getRepository() {
        return context.getBean(ISessionDTORepository.class);
    }

}