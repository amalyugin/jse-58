package ru.t1.malyugin.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class ProjectShowListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDTO> projectList;

    public ProjectShowListResponse(@Nullable final List<ProjectDTO> projectList) {
        this.projectList = projectList;
    }

}