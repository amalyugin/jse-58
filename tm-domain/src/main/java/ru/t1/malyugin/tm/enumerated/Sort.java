package ru.t1.malyugin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.comparator.WBSComparatorFunctional;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", WBSComparatorFunctional.NAME),
    BY_STATUS("Sort by status", WBSComparatorFunctional.STATUS),
    BY_CREATED("Sort by created", WBSComparatorFunctional.CREATED);

    @NotNull
    @Getter
    private final String displayName;

    @Nullable
    @Getter
    private final Comparator<?> comparator;

    Sort(@NotNull final String displayName, @Nullable final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public static String renderValuesList() {
        @NotNull final StringBuilder result = new StringBuilder();
        final int size = Sort.values().length;
        for (int i = 0; i < size; i++) {
            result.append(String.format("%d - %s, ", i, Sort.values()[i].displayName));
        }
        return result.toString();
    }

    @Nullable
    public static Sort getSortByIndex(final Integer index) {
        if (index == null || index < 0 || index >= Sort.values().length) return null;
        return Sort.values()[index];
    }

}